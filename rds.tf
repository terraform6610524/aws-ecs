resource "aws_db_instance" "name-?" {
  storage_type           = "gp2"
  allocated_storage      = 20
  multi_az               = false
  max_allocated_storage  = 1000
  engine                 = "mariadb"
  engine_version         = "10.5"
  instance_class         = var.dbbilling_stage_instance_size
  identifier             = "${var.project_name}-name-?"
  username               = "user"
  password               = "pass"
  backup_retention_period = 7
  publicly_accessible    = true
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet-group.id  
  vpc_security_group_ids = [aws_security_group.name-?.id]
  tags = {
    "Project" = var.project_name
  }

  lifecycle {
    ignore_changes = [password]
  }
}

resource "aws_db_instance" "name-?" {
  storage_type           = "gp2"
  allocated_storage      = 20
  multi_az               = false
  max_allocated_storage  = 1000
  engine                 = "mysql"
  engine_version         = "8.0.28"
  instance_class         = var.dbstore_stage_instance_size
  identifier             = "${var.project_name}-name-?"
  username               = "user"
  password               = "pass"
  backup_retention_period = 7
  publicly_accessible    = true
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.name-?.id  
  vpc_security_group_ids = [aws_security_group.name-?.id]
  tags = {
    "Project" = var.project_name
  }

  lifecycle {
    ignore_changes = [password]
  }
}

