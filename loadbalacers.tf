resource "aws_lb_target_group" "name-?" {
  name					= "${var.cluster_name_stage}-name-?"
  port					= 80
  protocol			= "HTTP"
  vpc_id				= module.vpc.vpc_id
  target_type		= "ip"
  health_check	{
    path = "/"
  }
}

resource "aws_lb_target_group" "name-?" {
  name          = "${var.cluster_name_stage}-name-?"
  port          = 80
  protocol      = "HTTP"
  vpc_id				= module.vpc.vpc_id
  target_type   = "ip"
  health_check  {
    path = "/"
  }
}

resource "aws_lb_listener" "name-?" {
  load_balancer_arn = aws_lb.name-?.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.name-?.arn
  }
}

resource "aws_lb" "name-?" {
  name               = "${var.cluster_name_stage}-lb"
  load_balancer_type = "application"
  subnets            = module.vpc.public_subnets
}
