
resource "aws_elasticache_cluster" "redis" {
  cluster_id           = "${var.project_name}-name-?"
  engine               = "redis"
  node_type            = var.redisbilling_stage_instance_size
  num_cache_nodes      = 1
  parameter_group_name = "default.redis5.0"
  engine_version       = "5.0.6"
  port                 = 6379
  subnet_group_name    = aws_elasticache_subnet_group.redis-subnet-group.name
  security_group_ids   = [aws_security_group.store-stage-redis-sg.id]
}