

resource "aws_ecs_cluster" "name-?" {
  name = "name-?"
}

resource "aws_ecs_service" "name-?" {
  name = "name-?"
  cluster = aws_ecs_cluster.filmable_stage.id
  task_definition = aws_ecs_task_definition.name-?.arn
  desired_count = 1
  launch_type = "FARGATE"
  load_balancer {
    target_group_arn = aws_lb_target_group.store-lb-tg.arn
    container_name   = "${var.cluster_name_stage}"
    container_port   = 80
 }  
  network_configuration {
    subnets = module.vpc.private_subnets
    security_groups  = []
    assign_public_ip = false
  }

}

resource "aws_ecs_service" "name-?" {
  name = "name-?"
  cluster = aws_ecs_cluster.filmable_stage.id
  task_definition = aws_ecs_task_definition.name-?.arn
  desired_count = 1
  launch_type = "FARGATE"
  
  network_configuration {
    subnets = concat(module.vpc.private_subnets, module.vpc.public_subnets)
    security_groups  = []
    assign_public_ip = false
  }

}

resource "aws_ecs_task_definition" "store" {
  family                   = "store"
  container_definitions    = jsonencode([

    {
       "environment": [
      {
        name  = "APP_SECRET",
        value =  "?"
      },
      {
        name  = "APP_VERSION_HASH",
        value = "?"
      },
      {
        name  = "APP_ENV",
        value = "?"
      },
      {
        name  = "APP_SECRET",
        value =  "?"
      },
      {
        name  = "PROJECT_NAME",
        value = "?"
      },
      {
        name  = "DATABASE_HOST",
        value = "?-stage.chuqei39avlj.eu-west-1.rds.amazonaws.com"
      },
      {
        name  = "DATABASE_PORT",
        value =  "?"
      },
      {
        name  = "DATABASE_NAME",
        value = "?"
      },
      {
        name  = "DATABASE_USER",
        value = "?"
      },    
      {
        name  = "DATABASE_PASSWORD",
        value = "?"
      },   
      {
        name  = "MAILER_TRANSPORT",
        value =  "?"
      }
      ]     
      
      name                     = "${var.cluster_name_stage}-name-?"
      image                    = "image"
      portMappings             = [
        {
         containerPort        = 80
         hostPort             = 80 
        }
      ]
    }
  ])
  
  task_role_arn      = "arn:aws:iam::......."
  execution_role_arn = "arn:aws:iam::......."
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
}

resource "aws_ecs_task_definition" "name-?" {
    family                 =  "name-?"
    container_definitions  =  jsonencode([
     {
       name                     = "${var.cluster_name_stage}-name-?"
       image                    = "nginx"

     }
    ])
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    task_role_arn      = "arn:aws:iam::987157480843:role/ecsTaskExecutionRole"
    execution_role_arn = "arn:aws:iam::987157480843:role/ecsTaskExecutionRole"
    network_mode       = "awsvpc"  
}
