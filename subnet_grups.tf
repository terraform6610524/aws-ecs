resource "aws_elasticache_subnet_group" "redis-subnet-group" {
  name       = "${var.project_name}-redis"
  subnet_ids = module.vpc.public_subnets
}

resource "aws_db_subnet_group" "rds-subnet-group" {
  name       = "${var.project_name}-subnetgroup"
  subnet_ids = module.vpc.public_subnets
}
