module "vpc" {

  source = "terraform-aws-modules/vpc/aws"
  name = "${var.project_name}-vpc"
  cidr = var.vpc_cidr_prod

  azs             = var.vpc_azs_stage
  private_subnets	= var.vpc_stage_private_subnets
  public_subnets	= var.vpc_stage_public_subnets

  enable_nat_gateway     = true
  enable_vpn_gateway     = false
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  enable_dns_hostnames   = true
  enable_dns_support     = true

  tags = {
    "Project" = var.project_name
  }
}




