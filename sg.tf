resource "aws_security_group" "name-?" {
  name = "${var.cluster_name_stage}-name-?"
  vpc_id = module.vpc.vpc_id
  ingress {
    description     = "allow http"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "name-?" {
  name = "${var.project_name}-name-?"
  vpc_id = module.vpc.vpc_id

  ingress {
    description     = "Jenkins"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks     = [""]
  }
  ingress {
    description     = "Jenkins slave1"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks     = [""]
  }
  ingress {
    description     = "VPN"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    prefix_list_ids = [var.pl_stage]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "name-?" {
  name = "${var.project_name}-name-?"
  vpc_id = module.vpc.vpc_id


  ingress {
    description     = "Jenkins"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks     = [""]
  }
  ingress {
    description     = "VPN"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    prefix_list_ids = [var.pl_stage]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  } 
 }


resource "aws_security_group" "name-?" {
  name = "${var.project_name}-name-?"
  vpc_id = module.vpc.vpc_id

  ingress {
    description     = "VPN"
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
    prefix_list_ids = [var.pl_stage]
  }
  ingress {
    description     = "${var.project_name}-name-?"
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]

  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "name-?" {
  name = "${var.project_name}-name-?"
  vpc_id = module.vpc.vpc_id                                       
  ingress {
    description     = "VPN"
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
    prefix_list_ids = [var.pl_stage]
  }
 ingress {
   description     = "${var.project_name}-name-?"
   from_port       = 6379
   to_port         = 6379
   protocol        = "tcp"
   cidr_blocks     = ["0.0.0.0/0"]
 }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "redis-sg" {
  name_prefix = "redis-sg"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}